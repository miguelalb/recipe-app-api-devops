variable "prefix" {
  type        = string
  default     = "raad"
  description = "A prefix to identify resources created for this project. Raad - Recipe App Api Devops."
}

variable "project" {
  type        = string
  default     = "recipe-app-api-devops"
  description = "Name of the project."
}

variable "contact" {
  type        = string
  default     = "Miguel Acevedo"
  description = "Project maintainer."
}